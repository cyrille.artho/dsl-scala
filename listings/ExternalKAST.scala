trait Exp
case class ParenExp(exp: Exp) extends Exp
case class DotExp(exp: Exp, ident: String) extends Exp
case class IdentExp(ident: String) extends Exp
