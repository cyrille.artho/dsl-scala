class ParamPrefix(f : PartialFunction[AbsEvent, Process]) extends Process {
    override def acceptPrim(e: AbsEvent): Process =
      if (f.isDefinedAt(e)) f(e) else Failure

    override def terminate() = Failure
}


object CSPE {
...
 def ??(f : PartialFunction[AbsEvent, Process]) = new ParamPrefix(f)
...
}