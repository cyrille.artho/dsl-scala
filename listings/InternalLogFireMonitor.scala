class LockOrder extends Monitor { 
  val acquire, release = event
  val Locked, Edge = fact

  "acquire" -- acquire('t, 'l)                    |-> insert(Locked('t, 'l))
  "release" -- Locked('t, 'l) & release('t, 'l)   |-> remove(Locked)
  "edge"    -- Locked('t, 'l1) & acquire('t, 'l2) |-> insert(Edge('l1, 'l2))
  "cycle"   -- Edge('l1, 'l2) & Edge('l2, 'l1)    |-> fail() }
