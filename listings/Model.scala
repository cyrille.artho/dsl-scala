object Model {
  /* implici\t conversion fo\r
     transition declaration */
  implicit def stringPairToTrans
    (tr: (String, String)) = {
    new Transition(tr._1, tr._2) }

  /* model data */
  val transitions =
    ListBuffer[Transition]()

  def addTrans(tr: Transition) {
    transitions += tr } }
