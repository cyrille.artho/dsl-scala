implicit def R(n\ame: String) = new { 
  def --(c: Condition) = new RuleDef(n\ame, List(c)) }

class RuleDef(n\ame: String, conditions: List[Condition]) { 
  def &(c: Condition) = new RuleDef(n\ame, c :: conditions)

  def |->(stmt: => Unit) { 
    addRule(Rule(n\ame, conditions.reverse, Action((x: Unit) => stmt))) } }
