import Model._

class Example {
  def action { /* code */ }

  "a" -> "b" := { action } }
