monitor LockOrder {
  acquire(t1,a) -> {
    release(t1,a) -> ok
    acquire(t1,b) :: !(b = a) -> {
      acquire(t2,b) -> {
        release(t2,b) -> ok
        acquire(t2,a) -> error } } } }
