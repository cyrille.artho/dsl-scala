trait Event
case class acquire(thread:String,lock:String) extends Event
case class release(thread:String,lock:String) extends Event

class LockOrder extends Monitor[Event] { 
  whenever { 
    case acquire(t1, a) => state { 
      case release(`t1`, `a`) => o\k
      case acquire(`t1`, b) if b != a => state { 
        case acquire(t2, `b`) => state { 
          case release(`t2`, `b`) => o\k
          case acquire(`t2`, `a`) => erro\r } } } } }
