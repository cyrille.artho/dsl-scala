implicit def convSingleState(state: state): Set[state] = Set(state)
implicit def convBool(b: Boolean): Set[state] = Set(if (b) o\k else erro\r)
implicit def convUnit(u: Unit): Set[state] = Set(o\k)
implicit def convStatePredicate(s: state): Boolean = states contains s
