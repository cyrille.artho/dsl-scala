val lock_order = process (p => ?? {
  case acquire(t1, a) =>
    p || ?? { case release(`t1`, `a`) => STOP
              case acquire(`t1`, b) if a != b =>
                ?? { case acquire(t2, `b`) =>
                  ?? { case release(`t2`, `b`) => STOP
                       case acquire(`t2`, `a`) => FAIL }}}})
