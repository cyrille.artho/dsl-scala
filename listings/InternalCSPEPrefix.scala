class Prefix(e0 : AbsEvent, p : Process) extends Process {
  override def acceptPrim(e: AbsEvent): Process =
    if (e == e0) p else Failure

  override def terminate() = Failure
}

abstract class Process {
  ...
  def ->: (e: AbsEvent): Process = new Prefix(e, this)
  ...
