class Loop (f : Process => Process) extends Process {
  override def acceptPrim(e : AbsEvent) : Process = f(this).accept(e)
  override def terminate () = f(this).terminate()
}

object CSPE {
  ... 
  def loop (f : Process => Process) = new Loop (f)
  ...
 }