grammar K;

classDeclaration:
  'clas\s' Identifier extend\s? '{' memberDeclarationList? '}';

assocDeclaration:
  'asso\c' Identifier  '{' assocMemberDeclarationList? '}';

expression:
  '(' expressio\n ')' #ParenExp
  | Identifier #IdentExp
  | expressio\n '.' Identifier #DotExp
...
