class Instrument {
  var id : Int
  var n\ame : String
  var powerLevel : Real }

class Spacecraft {
  var id : Int
  var instruments : [Instrument]
  req InstrumentsCount :
    instruments.count > 0 && 
    forall i,j:instruments . i != j => i.id != j.id }

assoc SpacecraftInstrument {
  part Spacecraft : Spacecraft
  part instruments: Instrument 1 .. 10 }
