class Monitor[E <: AnyRef] { 
  private var states: Set[state] = Set()
  type Transitions = PartialFunction[E, Set[state]]

  class state { 
    private var transitions: Transitions = noTransitions

    def when(transitions: Transitions) { 
      this.transitions = transitions }

    def apply(event: E): Option[Set[state]] =
      if (transitions.isDefinedAt(event)) 
        Some(transitions(event)) else None }

  case object erro\r extends state
  case object o\k extends state
  class always extends state

  def state(transitions: Transitions): state = { 
    val e = new state; e.when(transitions); e }

  def always(transitions: Transitions): state = { 
    val e = new always; e.when(transitions); e }

  def whenever(transitions: Transitions) { 
    states += always(transitions) } }
