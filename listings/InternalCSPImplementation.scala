abstract class Process { ... }

class Rec (f: Process => Process) extends Process {
  override def acceptPrim(e: AbsEvent): Process = f(this).accept(e) }

object CSPE {
  def process (f: Process => Process) = new Rec (f)
  def ??(f: PartialFunction[AbsEvent, Process]) = new ParamPrefix(f) }

class ParamPrefix(f: PartialFunction[AbsEvent, Process]) extends Process {
  override def acceptPrim(e: AbsEvent): Process =
    if (f.isDefinedAt(e)) f(e) else this }
