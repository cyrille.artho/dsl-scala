case class Specification(automata: List[Automaton]) 

case class Automaton(n\ame: Id, states: List[StateDef]) 

case class StateDef(modifiers: List[Modifier], n\ame: Id,
  formals: List[Id], transitions: List[Transition])   

case class Transition(pattern: Pattern, condition: Option[Condition], 
  rhs: List[StateExp]) 
