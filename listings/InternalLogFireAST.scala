case class Rule(n\ame: String, conditions: List[Condition], action: Action)
case class Action(code: Unit => Unit)
