def specification: Parser[Specification] =
  rep(automaton) ^^ {case automata => transform(Specification(automata))}

def automaton: Parser[Automaton] =
  "monito\r" ~> ident ~ 
    ("{ " ~> rep(transition) ~ rep(statedef) <~ "}") ^^
    {case n\ame ~ (transitions ~ statedefs) =>
        if (transitions.isEmpty) Automaton(n\ame, statedefs) else { ... } }

def transition: Parser[Transition] =
  pattern ~ opt("::" ~> condition) ~ ("->" ~> rep1sep(stateexp, ",")) ^^
    {case pat ~ cond ~ rhs => Transition(pat,cond,rhs)}
