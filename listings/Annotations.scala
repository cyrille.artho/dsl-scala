@Doc("overrides environment variable CLASSPATH i\f set")
var classpath: String = "."

@Choice(Array("one", "two", "many"))
var simpleNumber = "one"

@Range(dmin = 0.0, dmax = 1.0) @Shorthand('p')
var defaultProbability = 0.5
