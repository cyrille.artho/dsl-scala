import scala.collection.mutable.ListBuffer

object Model {
  implicit def stringPairToTransition(names: (String, String)) = {
    new Transition(names._1, names._2)
  }

  def main(args: Array[String]) {
    new Example()
    Console.out.println(transitions.toList.mkString(", "))
  }

  val transitions = ListBuffer[Transition]()

  def registerTransition(trans: Transition) {
    transitions += trans
  }
}
