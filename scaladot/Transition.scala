class Transition(val src: String,
		 val tgt: String) {
  var transfunc: () => Any = null

  def := (action: => Any) {
    transfunc = () => action
    Model.registerTransition(this)
  }

  override def toString = {
    src + " -> " + tgt
  }
}
