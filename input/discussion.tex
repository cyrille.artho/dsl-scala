
\section{Discussion\label{sec:discussion}}

We discuss the characteristics of five approaches to DSL
implementation from our experience with DSLs for formal modeling
(see Table~\ref{fig:dsl-pros-cons} for a summary).

\newcommand{\yes}{$\surd$}
\newcommand{\no}{$\times$}
\newcommand{\tbd}{$\otimes$}

\begin{table}
\caption{Characteristics of different DSL implementation
approaches. Signature:
\yes{} means yes, \no{} means no, \tbd{} means no unless an effort is
made to make it happen, and $++$, $+$, $-$, $--$ rank different
approaches from best to worst.
\label{fig:dsl-pros-cons}}
\begin{tightcenter}
{
\renewcommand{\arraystretch}{1.05}% default is 1.0, a bit tight for some symbols
\setlength{\tabcolsep}{6pt}
% default is 6pt but Springer uses a tighter layout
% that does not work so well with text/symbols
\begin{tabular}{l|c c|c c c}
%\cline{2-6}
& \multicolumn{2}{|c|}{External} &
%\multirow{2}{*}{Annotations} & 
\multicolumn{3}{c}{Internal} \\
\cline{2-3}\cline{4-6}
& tool & lib & annotations & deep & shallow\\
% & External tool & External lib & Annotations & Internal deep & Internal shallow \\ 
 \hline
 Parser generation & \yes & \no & \no & \no & \no \\  
 \hline
 AST generation & \yes & \yes & \yes & \yes & \no \\ 
 \hline
 Enables transformation, analysis & \yes & \yes & \yes & \yes & \no \\ 
 \hline
 Directly executable & \no & \no & \no & \no & \yes \\ 
 \hline
 Turing complete & \tbd & \tbd & \no & \yes & \yes \\ 
 \hline
 Ease of development & $--$ & $-$ & $++$ & $+$ & $++$ \\ 
 \hline
 Flexibility of syntax & $++$ & $++$ & $--$ & $-$ & $--$ \\ 
 \hline
 Quality of syntax error messages & $+$ & $+$ & $++$ & $--$ & $-$ \\ 
 \hline
 Ease of use & $++$ & $++$ & $+$ & $-$ & $--$ \\
\end{tabular}
}
\end{tightcenter}
\end{table}

\newcommand{\etool}{{external tool}}
\newcommand{\elib}{{external lib}}
\newcommand{\Elib}{{External lib}}
\newcommand{\ideep}{{internal deep}}
\newcommand{\ishallow}{{internal shallow}}

\paragraph{Parser generation} The \etool{} approach
requires an extra {\em parser generation} and compilation stage, where
a parser is first generated from a grammar specification, and then
compiled.  The other approaches have no code generation stage, which
slightly facilitates development.

\paragraph{AST generation} All approaches except the \ishallow{}
approach generate ASTs. AST generation can complicate matters and at
the same time be a facilitator. It influences topics such as
transformation analysis, executability, Turing completeness, ease of
development, and flexibility of syntax, as discussed below.

\paragraph{Enables transformation and analysis}
An AST allows us to transform and analyze the
DSL. 
Specifically, it allows us to optimize any code generated from the AST,
a capability particularly important for the shown monitoring DSLs.
This is not directly possible in the \ishallow{} approach, which
is one of its main drawbacks.  One could potentially
use reflection, but the Scala reflection API does not support
reflection on code.  Alternatively one can use a bytecode analysis
library such as ASM~\cite{bruneton02} or the Scala compiler
plugin. However, these solutions require a great level of
sophistication of the developer.
\begin{comment}
\textbf{RK: what is this?} DSL implementations may be mixed, with deep
embedding used where code analysis is important.  As the reader may
recall, the left-hand sides of rules in the \logfire{} DSL were deep
DSLs which was necessary in order to optimize rule execution according
to the Rete algorithm.  The right-hand sides were not used for
optimization, and here there more expressive the better, which is why
actions were shallow DSLs: any Scala code can be provided as an
action. The data automaton DSL is a purely shallow DSL, which means
that it is very difficult to optimize these monitors for fast event
processing.
\end{comment}

\paragraph{Directly executable} The \ishallow{} approach has the
advantage that the DSL is directly executable since the host language
models the DSL. There is no AST to be
interpreted/translated. This again means that \ishallow{} DSLs are
faster to develop, often requiring orders of magnitudes less code.

\paragraph{Turing complete} Annotations typically carry simple data, 
and are usually not Turing complete (although they can be made to be). 
Internal DSLs are by definition
Turing complete since they extend a Turing complete host language (in
our case, Scala).  For \ishallow{} DSLs the host language is directly
part of the DSL, thus making the DSL itself Turing complete. Our
experience with internal DSLs is that the user of the DSL will
use the host language constructs in case the DSL is not applicable to
a particular problem.  As an example, an internal DSL lends itself
to writing ``glue code'' to connect the DSL with another system, such
as the system under test in case of a test DSL.  
It is more challenging to turn external DSLs into Turing complete languages.

\paragraph{Ease of development} External DSLs developed using a library (such as Scala's parser combinators) seem easier to
develop than using a parser generator tool (such as ANTLR) 
due to the reduced parser generator step.
However, using a parser generator such as ANTLR facilitates
grammar development itself since ANTLR accepts more grammars
than for example Scala's parser combinators. 
%In the case of ANTLR, however, the extra translation step from Java to Scala adds
%development overhead.
Annotations-based DSLs are easy to develop
since the Java compiler and the core libraries support
annotations. However, it is not possible to extend the syntax or scope
of annotations in any way. It furthermore appears that internal DSLs
are easier to develop than external DSLs, and that \ishallow{} DSLs
are the easiest to develop.

\paragraph{Flexibility of syntax} Our experience with internal
DSLs is that it can be a struggle to achieve the optimal syntax.
This is mostly due to limitations in operator composition and precedence
in Scala. In an external DSL one is completely free to
create any grammar as long as it is accepted by the parser.

\paragraph{Quality of syntax error messages} External DSLs have
a potential for good error messages, depending on the toolkit used.
Internal DSLs often result in error messages that can be
intimidating to users, especially if not used to the host
language. In the case of deep internal DSLs, conversion
functions may show up in compiler errors; or a missing
symbol may result in a lack of conversion, in which case no error
message is shown or a completely wrong one. 
Furthermore, for a deep internal DSL a type checker has to be developed
from scratch. In contrast, shallow internal DSLs have the advantage that 
Scala's type system takes care of type checking.   
%Resulting programs may be well-typed but if a type conversion is missing, the desired action of the affected statement will not be executed.

\paragraph{Ease of use} Annotations and internal DSLs are usually adopted
easily if the users are already host language
programmers. As an illustration, in spite of being originally a research tool,
\tracecontract{}~\cite{tracecontract-fm11}, a variant of the internal data automaton DSL illustrated in Section~\ref{sec:int-shallow-daut}, was
used throughout NASA's LADEE Moon mission for checking all command
sequences before being sent to the spacecraft~\cite{dsl-scaladays-11}. Similarly,
the internal DSL \logfire{}~\cite{havelund-logfire-sttt14}, illustrated in Section~\ref{sec:int-deep-logfire}, also originally a research tool,  is currently used daily for checking telemetry from JPL's Mars Curiosity Rover~\cite{havelund-joshi-ftscs-2014}. These adoptions by NASA missions were not
likely to have happened had these DSLs been external limited stand-alone
languages.
%, as long as no deep nesting of (arrays of) annotations is needed. 
On the other hand, if a user is not already a host language programmer (and is not willing to learn the host language), it may
be easier to adopt an external DSL. 
%We have attempted to
%introduce internal Scala DSLs to groups of people, some of which were
%not Scala programmers. Introducing a new programming language to a
%group of users is a challenging task.
For example, we developed an
external monitoring DSL much along the lines of data automata, and had
non-programmers use it for testing without much
training~\cite{logscope-aiaa-10}. More interestingly perhaps, the SysML-inspired
external modeling DSL \Klang{}, illustrated in Section~\ref{sec:ext-k},
is planned to be adopted by a JPL's future mission 
to Jupiter's Moon Europa, for modeling mission scenarios. 


%Ease of use is also directly related to the quality of
%syntax error messages and the flexibility of syntax, as discussed above.

%The easier it is to customize the syntax according to needs, the
%easier it may be to use the DSL.

%KH - this does not seem relevant:
%Scala's compilation to bytecode has been stable over the last few
%years but is going to change with Scala 2.12/Java 8
%code inside code blocks

%an experimental deep DSL  
%has been developed on top of LogFire
%in order to reduce the size of specifications \cite{havelund-joshi-icfem-2014}.

