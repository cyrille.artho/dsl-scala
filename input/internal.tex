
\section{Internal DSLs\label{sec:internal-dsl}}

An internal DSL extends the host language with custom constructs.
We cover three variants: Annotations, shallow embedding, and deep embedding.

With {\em annotations,} a host program is
annotated with information on existing language constructs. The
host language needs to permit annotations, as is the case for Java
and Scala. When using embedding,
the extension is implemented as a library in the host language without
any additional techniques. Here we distinguish between shallow and deep
embedding. \emph{Shallow embedding} uses the host language's features
directly to model the DSL. The
constructs have their usual meaning. In contrast, in a \emph{deep
  embedding} one creates a separate representation of the DSL: an
abstract syntax (AST), which is then interpreted or translated, as in the
case of an external DSL.
%As a consequence a shallow embedding directly implements a fixed semantics of the DSL,
%while a deep embedding typically converts the elements of the DSL to
%a data structure to decouple the implementation from the language itself.
%
%We do not cover {\em macros}, which can also be used to extend the host
%language. Macros span from simple macro systems such as
%Scala's macro system to advanced compiler-based language extension
%frameworks such as SugarScala~\cite{jakob2014}.
%THIS HAS ALREADY BEEN SAID IN THE INTRODUCTION.


\subsection{Annotations}

Annotations in Java or Scala associate extra
information with classes, fields, and methods.
%Java annotations were introduced in version 1.5 of
%the Java programming language~\cite{GoslingJoySteeleBracha2000}.
%\subsubsection*{Technical background}
Java annotations accept a possibly empty list of key-value pairs of
parameters, where parameters can be of primitive types, strings,
classes, enumerations, and arrays of the preceding types.
%Annotations
%can extend the target language, and provide information at
%compile-time or at run-time.
To be used at run-time with
Scala, annotations have to be defined as Java annotations with
run-time retention since Scala's annotations currently do not persist
past the compilation stage.
%
%At run-time, annotations are accessed via reflection using
%method \texttt{getAnnotation}~\cite{java-annotations}.

%Because annotations only allow arguments that can be
%computed at compile-time, almost no relevant syntactic information is lost.%
%\footnote{The order in which multiple annotations or parameters were provided,
%cannot be retrieved at run-time.}

%One of the first Java extensions to provide annotations was the
%Java Modeling Language
%(JML)~\cite{vandenberg01loop}. In this language, annotations describe
%properties of variables and code, such as parameter ranges or loop invariants.
%JML uses its own syntax, where annotations are embedded
%in Java comments as an external DSL.
%
%Unlike JML properties, which have a rich syntax, the grammar of Java
%annotations is limited.
%Java annotations may be nested or listed next to each
%other, but it is not possible to define a full DSL based on them; for example,
%expressions like \texttt{@requires x >= 0.0} would have to be embedded as
%text in a Java annotation.

\subsubsection*{Example: Modbat's configuration engine}

Many command line programs
allow options to be set via environment variables or command line arguments.
Existing libraries parse command line arguments, but
still leave it to the user to check semantic correctness, such as
whether a value is within a permitted range. This problem also occurs
for Modbat, a model-based test tool~\cite{artho-hvc2013}.

\begin{figure}
\includegraphics[scale=\scaleFactor]{listings/Annotations}
\caption{Example of annotations for configuration variables.\label{fig:mbt-config}}
\end{figure}

In Modbat, configuration options are expressed as annotated Scala
variables. An internal library analyzes the annotations
and parses command
line arguments, checking whether a parameter matches the name of an annotated
variable. If so, the default value of the right
variable is overridden by the new value. For example, the value for
\texttt{defaultProbability} can be overridden by command line option
\texttt{-{}-default-probability=0.6}.
%
%Annotations to configuration variables define attributes or permitted
%values of configuration options.
Figure~\ref{fig:mbt-config} shows an example.
\texttt{@Doc} provides a documentation string for a usage/help output;
\texttt{@Choice} limits options to a predetermined set;
\texttt{@Range} defines the allowed range of a value; and
\texttt{@Shorthand} defines a one-letter shorthand for a command line
option.
% Other annotations (not shown here) are used for advanced features

%such as testing.% and pretty-printing.

\begin{comment}
\begin{table}
\caption{Key features of Modbat's configuration engine.}
\begin{tightcenter}
\begin{tabular}{ll}
Annotation & Usage\\\hline
\texttt{@Doc}&Documentation string (for help screen)\\
\texttt{@Choice}&Value must be member of a given set\\
\texttt{@Range}&Range limitation (optional: \texttt{min}, \texttt{max})\\
\texttt{@Shorthand}&Command line shortcut (alternative to long name)\\
\texttt{@Test}&Override default value for self-test\\
\texttt{@Hex}&Show current configuration value in hexadecimal notation\\
\end{tabular}
\end{tightcenter}
\end{table}
\end{comment}

It is also possible to use inheritance, naming
conventions, or library calls, to represent or set certain
attributes. However, these solutions are more limiting than
annotations or require more code. Indeed, when Java annotations
became available, many tools (such as JUnit~\cite{junit}) adapted them.

\subsection{Shallow embedding}

In a shallow embedding the host language's features are used directly,
with their usual meaning, to model the DSL, which is typically presented as an
application programming interface (API).

%The DSL is then provided as a library defining the extended
%syntax, and compiled by the Scala compiler. At run-time the code is executed
%like a normal Scala or Java program.

%\subsubsection{Technical background}

\subsubsection*{Example: Data automata\label{sec:int-shallow-daut}}

Recall the lock order monitor expressed in an external DSL in Figure
\ref{fig:ext-lock-order-monitor}. Figure
\ref{fig:int-shallow-daut-lock-order-monitor} shows a version of this
monitor expressed in an internal shallow DSL, also described in
\cite{havelund-isola-2014}, and variants of which are described in
\cite{tracecontract-fm11,havelund-tase-2014}.
%
Event types are defined as case classes sub-classing a trait
\code{Event}.\footnote{A trait in Scala is a module concept closely
  related to the notion of an abstract class, as for example found in
  Java. Traits, however, differ by allowing a more flexible way of
  composition called mixin composition, an alternative to multiple
  inheritance.}
The monitor itself is defined as a class sub-classing
the trait \code{Monitor}, which is parameterized with the event
type. The trait \code{Monitor} offers various constants and methods
for defining monitors, including in this case the methods
\code{whenever} and \code{state}, and the constants \code{ok} and
\code{error}, which are not Scala keywords. The \code{LockOrder}
monitor looks surprisingly similar to the one in Figure
\ref{fig:ext-lock-order-monitor}.
%\ref{fig:int-shallow-daut-lock-order-monitor}.

\begin{figure}
\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalDautMonitor}
\caption{Internal shallow Scala DSL: lock order event definitions and monitor.
\label{fig:int-shallow-daut-lock-order-monitor}}
\end{tightcenter}
\end{figure}

The complete implementation of the internal DSL is less than 200
lines of code, including printing routines for error messages.
Conversely, the similar external DSL is approximately
1500 lines (nearly an order of magnitude more code) and less
expressive.  The parts of the implementation of the internal shallow
DSL directly relevant for the example in Figure
\ref{fig:int-shallow-daut-lock-order-monitor} are shown in Figure
\ref{fig:int-shallow-daut-implementation}.

A monitor contains a set of currently active states, the
{\em frontier.} All the states have to lead to success (conjunction
semantics). A transition function is a {\em partial function} (a Scala
concept), which maps events to a set of target states. A state object
(of the case class \code{state}) contains a transition function, which
is initialized with the \code{when} function. 
A state is made to react to an event using \code{apply}.%
\footnote{The \code{apply} method in Scala has special interpretation: if an
object \code{O} defines a such, it can be applied to a list of
arguments using function application syntax: \code{O(\dots)},
equivalent to calling the \code{apply} method: \code{O.apply(\ldots)}.}
There are several forms (sub-classes) of states,
including \code{error}, \code{ok}, and \code{always} states that stay
active even if the transition function applies to an event.
Functions \code{state} and \code{always} take a transition function and
return a new state object with that transition function.
Function \code{whenever} creates an \code{always} state from the transition
function and adds it to the set of initial states of the monitor.

\begin{figure}
\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalDautMonitorImpl}
\caption{Internal shallow Scala DSL: part of implementation.
\label{fig:int-shallow-daut-implementation}}
\end{tightcenter}
\end{figure}

The type of a transition function suggests that it returns a
set of states. In Figure
\ref{fig:int-shallow-daut-lock-order-monitor}, however, the result of
transitions (on the right of \code{=>}) are single states, not sets of
states. This would not type check was it not for the definition of the
implicit function \code{convSingleState}, which lifts a single state
to a set of states, here shown together with a selection of other
implicit conversion functions:

\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalDautImplicits}
\end{tightcenter}

These other implicit functions support lifting for example a Boolean
value to an \code{ok} or \code{error} state (such that one can write a
Boolean expression on the right-hand side of \code{=>}); the
\code{Unit} value to \code{ok} (such that one can write statements
with side-effects on the right-hand side); and finally a state to a
Boolean, testing whether the state is in the set of active states,
used in conditions.


\subsubsection*{Example: \CSPE{}}

\begin{figure}
\textbf{Example monitor:}\\
\vspace{2mm}
%\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/Process}\\
%\end{tightcenter}
%\vspace{.25\baselineskip}
\textbf{\CSPE{} implementation:}\\
% \begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalCSPImplementation}
%\caption{The definition of \code{loop}, \code{??}, and \code{:->} in \CSPE{}.\label{fig:csp-e-implementation}}
% \end{tightcenter}
\caption{Example monitor written in \CSPE{} and partial implementation.\label{fig:csp-e}}
\end{figure}

\CSPE{} (\CSP{} for events) is a run-time verification tool that uses a
notation similar to Hoare's Communicating Sequential Processes
(CSP)~\cite{Hoare1983}.  \CSPE{} allows specification of
``concurrent'' processes.  The top of Figure~\ref{fig:csp-e} shows the
lock order monitor in \CSPE{}.  Compared to the lock monitor of
Figure~\ref{fig:ext-lock-order-monitor}, the major difference is that
parallel composition of the top level process (\code{p ||
  ...}) is required to run the monitor continuously.  The similarity
to data automata in the previous example is evident.  The role of
\code{p || ...} is similar to \code{always} and \code{process} is
similar to \code{whenever} in data automata.  The pattern match clauses
are almost the same, except for \code{??}.

In \CSPE{}, recursive definitions are implemented by functions
that take a process and return a new process
(see Figure~\ref{fig:csp-e}, bottom).
Function \code{??} takes a partial function
from events to processes (monitors), and creates a new process.
That process evaluates the given function if it is defined for a
given event, and waits otherwise.
The monitor specification supplies the event as the first argument of a
process (before
\code{=>}) and the behavior of the process to be executed after receiving
the event, as the second one. Internally \code{acceptPrim} takes the first
argument and executes \code{f} to continue monitoring the right-hand side
of the expression.

\CSPE{} is implemented as an internal DSL. The main reason for this is
to interface with external logging tools.  The shallow
embedding furthermore simplifies the implementation.  However, due to
this,
%the operator
%precedence and associativity of \CSPE{} are those of Scala. 
the grammar of \CSPE{} slightly deviates from the
standard \CSP{} notation.
%For example, we have to append \code{:} to the
%prefix operator \code{->}, to make it right-associative. 
For example, parametric events and the recursive definition of processes are more
complicated than in standard \CSP{}.

\subsection{Deep embedding}

In a deep embedding, a DSL program is represented as an abstract
syntax tree (AST), which is then interpreted or translated as in the
case of an external DSL. The AST is generated via an API. We shall show two deep DSLs, one for writing state
machine driven tests, and one for rule-based trace monitoring.

\subsubsection*{Example: Modbat}

\begin{figure}
\begin{tabular}{lll}
\begin{minipage}[t]{.48\linewidth}
\textbf{Example.scala:}
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/Example}
\vspace{.25\baselineskip}
\textbf{Transition.scala:}
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/Transition}
\end{minipage}
&~\hspace{1mm}~&
\begin{minipage}[t]{.48\linewidth}
\textbf{Model.scala:}
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/Model}
\end{minipage}
\end{tabular}
\caption{Implementation of a deep DSL: a miniaturized version of Modbat,
given as an example (\textbf{Example.scala}) and two classes showing a
partial implementation.
\label{fig:modbat-example}}
\end{figure}

\begin{comment}
% example is very similar to Modbat example
\begin{figure}
\begin{alltt}
"A Stack" should "pop values in last-in-first-out order" in \{
  val stack = new Stack[Int]
  stack.push(1)
  stack.push(2)
  stack.pop() should be (2)
  stack.pop() should be (1)
\}
\end{alltt}
\end{figure}
\end{comment}

The model-based test tool Modbat generates test cases from extended
finite-state machines~\cite{artho-hvc2013}. Modbat has been
used to verify a Java model library and a SAT
solver~\cite{artho-hvc2013}. A Modbat model
contains definitions of transitions: source and target states, and
transition actions (code to be executed when a transition is taken).
Figure~\ref{fig:modbat-example} shows a simple example model and a
minimalist implementation that registers the model data at run time.
The key Scala features that are used for deeply embedding the DSL are
the definition of a custom operator \texttt{:=} in Transition.scala,
together with an implicit conversion of a string pair
\texttt{"a" }\raisebox{0.2ex}{\texttt{-}}\texttt{> "b"},
to a transition (in Model.scala).

\begin{comment}
\begin{figure}
\begin{tabular}{lll}
\begin{minipage}{.48\linewidth}
\begin{alltt}
"bound" -> "connected" := \{
    require(channel.isBlocking())
    startClient
    connection = ch.accept()
\} label "accept"
\end{alltt}
\end{minipage}
&~\hspace{1cm}~&
\begin{minipage}{.4\linewidth}
\begin{alltt}
@After def cleanup() \{
    if (connection != null)
        connection.close()
\}

\end{alltt}
\end{minipage}
\end{tabular}
\caption{Part of a Modbat model to test the server-side network API.
  Internally, the pair of strings is converted into a state pair,
  which is then augmented with a transition function, to which
  optional attributes such as labels are attached. Annotated functions
  provide integration with test harness code.
%\label{fig:modbat-example}
}
\end{figure}
\end{comment}

The design of Modbat's DSL mixes deep embedding for
transitions, with annotations and shallow embedding for code
representing transition actions on the system under test.
The main goal is to make
the syntax more declarative and concise where possible (e.\,g., to
declare transitions), while avoiding too many new constructs when
Scala code is used (hence the use of annotations and API functions
where appropriate).  Shallow embedding is ideal for transition actions
as they have to interact with the Java run-time environment during
test execution.

\subsubsection*{Example: LogFire\label{sec:int-deep-logfire}}

\logfire{} is an internal (mostly) deep Scala DSL~\cite{havelund-logfire-sttt14}.
It was created
for writing trace properties, as were the earlier described data
automata DSLs in Sections \ref{sec:external-lib} and
\ref{sec:int-shallow-daut} respectively. \logfire{} implements the Rete
algorithm \cite{forgy-rete-82}, modified to process
instantaneous events (in addition to facts that have a life span), and
to perform faster lookups in a fact memory.
A monitor is specified as a set of rules, 
each of the form: 

\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/Rule}
\end{tightcenter}

%\[
%  \textit{name}\ \verb+--+\ \textit{condition}_1\ \& \ldots \&\ \textit{condition}%_n\ \longmapsto\  \textit{action}
%\]

\begin{figure}
\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalLogFireMonitor}
\caption{Internal deep Scala \logfire{} DSL: lock order monitor.
\label{fig:int-deep-logfire-lock-order-monitor}}
\end{tightcenter}
\end{figure}

Figure
\ref{fig:int-deep-logfire-lock-order-monitor} illustrates the lock
order property expressed as rules in \logfire.
The rules operate on a database of facts, the {\em fact memory}. Rule
left-hand sides check incoming events, as well as presence or
absence of facts in the fact memory. Right-hand sides (actions) can
modify the fact memory, issue error messages, and
generally execute any Scala code (here the DSL becomes a shallow DSL).
%
Class \code{Monitor} defines features for writing rules, for example
the functions: \code{event}, \code{fact}, \code{-}\code{-}, \code{\&},
\code{|}\raisebox{.2ex}{\texttt{-}}\code{>},
\code{insert}, \code{remove}, and \code{fail}. Recall that in Scala,
method names can be sequences of symbols, and dots and parentheses
around method arguments are optional. Each rule definition in the
monitor above is a sequence of method calls, that last of which is the
call of the method \code{|}\raisebox{.2ex}{\texttt{-}}\code{>},
which produces an internal
representation (an abstract syntax tree) of the rule as an object of a
class \code{Rule}, which is then passed as argument to a method
\code{addRule(rule: Rule)} in the Rete module. The abstract syntax
class \code{Rule} is in part defined as:

\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalLogFireAST}
\end{tightcenter}
 
A rule consists of a name; a left-hand side, which is a list of
conditions, interpreted as a conjunction; and a right-hand side,
which is an action. Conditions use deep embedding for optimization purposes.
Actions are implemented as Scala code using a shallow embedding 
(functions from \code{Unit} to \code{Unit}).
% 
%The methods \code{event} and \code{fact} use Java's reflection to bind symbols (quoted names) to unquoted names. The first two lines of class \code{LockOrder} are equivalent to:
%
%\begin{lstlisting}[language=scala]
%  val acquire = 'acquire
%  val release = 'release
%  val Locked = 'Locked  
%  val Edge = 'Edge
%\end{lstlisting}
%
%LogFire uses symbols, single quoted names, for event and fact names. However, even single quoted names are undesirable in a DSL, and therefore the approach above to define unquoted names.
%

\begin{figure}
\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalLogFireRuleImpl}
\caption{Internal deep Scala \logfire{} DSL: rule syntax implementation.
\label{fig:int-deep-logfire-rule-implementation}}
\end{tightcenter}
\end{figure}

The definitions in Figure
\ref{fig:int-deep-logfire-rule-implementation} support the
transformation of a rule entered by the user to an AST object of the
class \code{Rule}.  The implicit function \code{R}, lifts a string (a
rule name) to an anonymous object. That object defines the 
\code{-}\code{-} operator, which when applied to a condition returns an object 
of the
class \code{RuleDef}. This class in turn defines the condition
conjunction operator \code{\&} and the action operator
\code{|}{\raisebox{.2ex}{\texttt{-}}\code{>}
defining the transition from left-hand side to right-hand side of the
rule. This operator calls \code{addRule}, which adds the rule to the
Rete network.  The implicit function \code{R} gets invoked by the
compiler automatically when a string is followed by the symbol
\code{-}\code{-}, to resolve the type ``mismatch'' (as no
\code{-}\code{-} operator is defined on strings).
The individual conditions in a rule are similarly constructed with the
help of the following implicit function, which lifts a symbol (the
name of an event or fact) to an object, which defines an \code{apply}
function:

\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalLogFireCondImplicit}
\end{tightcenter}

\noindent
The complete interpretation by the Scala compiler of the rule
\code{"release"} in Figure
\ref{fig:int-deep-logfire-lock-order-monitor} becomes:

\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/InternalLogFireRuleInterpretation}
\end{tightcenter}

