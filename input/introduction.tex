
\section{Introduction}

A domain-specific language (DSL) is a language specialized to a
particular domain \cite{fowler-dsl-10}. 
DSLs are for example popular in the formal methods and testing communities. 
%where many specialized modeling languages exist.
%A DSL allows a programmer to express a solution in a limited language. 
A DSL is designed to make the modeling or programming task easier and sometimes
eliminates the need for a full-fledged programming language
altogether. DSLs are classified into external and internal DSLs. An
{\em external DSL} is a stand-alone language with a customized parser.
Examples are XML~\cite{bray1998extensible} for representing data and
DOT~\cite{Gansner:2000:OGV:358668.358697} for drawing graphs, 
for example state machines.
%Promela~\cite{Holzmann04} and NuSMV~\cite{NuSMV} for representing
%models, and the Java Modeling Language~\cite{vandenberg01loop} for
%annotating Java code.  \issue{KH}{I am not sure I would mention NuSMV
%as well as Promela, perhaps none of them. Not sure about JML
%neither.}
In contrast to this, an {\em internal DSL} extends an existing programming
language, the \emph{host language.} A user may employ the host language
in addition to the DSL. On the
implementation side, the compiler and run-time environment of the host
language are reused. Internal DSLs are furthermore divided into
shallow and deep embeddings.
In a \emph{shallow embedding,} the host language's features are used
directly to model the DSL. The constructs
have their usual meaning. Conversely, in a \emph{deep embedding,} a
separate internal representation is made of the DSL (an abstract
syntax), which is then interpreted or compiled as in the case of an
external DSL.  The reuse of an existing programming language reduces
development time but can also manifest itself as a constraint for an
internal DSL.

Both external and internal DSLs therefore have their own
trade-offs. In this paper, we systematically examine these advantages
and disadvantages, based on our own practical experience in building
numerous external as well as internal modeling and testing DSLs with 
the Scala programming language. Scala is a modern strongly typed programming language combining object-oriented and functional
programming~\cite{Odersky2008}. Scala has several libraries and features
that make it suited for DSL development. For external DSLs these include
parser combinators, and for internal DSLs these include
implicit function definitions, which allow values of one type to be
lifted to values of a different type, the ability to call methods on
objects without dots and parentheses, case classes, partial functions,
call-by-name, user-defined operators composed of symbols, operator
overloading, as well as other features such as higher-order functions
and lambda expressions. 
 
Our contributions are: (a) a report on our experiences on using Scala to
implement external and internal DSLs for formal modeling, three of which
have been incorporated into real production systems, (b) a summary of the
pros and cons of each type of DSL, and (c) a survey of the various tools and
techniques used for creating DSLs with Scala.
%
A higher-level contribution is the message that Scala, 
as a high-level programming language, can be used for modeling
in general, possibly augmented with internal modeling DSLs.

%succinct and sound methodology for determining the type of DSL to be
%created based on requirements.

%\subsection{Related Work}
%\cite{flanagan2008ruby}
%\cite{van2007python}

Much work has been carried out studying the development of DSLs 
\cite{Mernik:2005:DDL:1118890.1118892,fowler-dsl-10}.
The literature is too vast to be surveyed here.  Other 
programming languages have been popular as a platform for DSLs, such as Ruby, 
Python, and Haskell.
One of our through-going themes is DSLs for event monitoring. 
Such internal DSLs have also been developed in Haskell \cite{stolz-rv-04} and Java 
\cite{bodden-mopbox-rv11}. Focusing specifically on Scala, an internal DSL for
rule-based programming, which we will also present, is presented in
\cite{hammurabi-scaladays-11}. Numerous internal DSLs have been developed in 
Scala for testing, including for example ScalaTest \cite{scalatest}.
Technologies that we have not yet explored,
and therefore will not evaluate, include Scala macros \cite{scala-macros}, 
Scala virtualized \cite{scala-virtualized}, and SugarScala \cite{jakob2014}, a 
framework for extending Scala's syntax.

The paper is organized as follows: 
Section~\ref{sec:external-dsl} covers the design and implementation of
external DSLs, while Section~\ref{sec:internal-dsl} covers internal
DSLs.  Section~\ref{sec:discussion} compares the key points of
different approaches, and Section~\ref{sec:conclusions} concludes.

%\nocite{wampler2009programming}.
