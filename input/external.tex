
\section{External DSLs\label{sec:external-dsl}}

An external DSL is a stand-alone language.
%, developed solely for the target domain.
With Scala, one can develop an external DSL using either
a {\em parser library} or a {\em parser tool.}

%An external DSL is a stand-alone language with a customized grammar. 
%In the context of Scala, the ways in which a grammar can be defined can
%be classified into two approaches: the {\em parser library} 
%approach and the {\em parser tool} approach.
%These will be discussed below.

\subsection{The parser library approach\label{sec:external-lib}}

%Parser libraries are used like any other Scala libraries.
Existing parser libraries for Scala include {\em
  parser combinators}~\cite{parser-combinators,gll-combinators} and {\em
  parsing expression grammars}~\cite{parboiled}.
A parser combinator is a higher-order function that accepts zero
or more parsers and returns a new parser. A
parser is a function that accepts a string and returns a
user-defined data object, typically a parse tree. Parser combinators are
based on recursive descent and facilitate modular 
construction.\footnote{The standard Scala library~\cite{scala} originally included a parser
combinator package. However, because it is rarely used, it
was moved to an external library \cite{parser-combinators}.}
%
Generalized LL (GLL) combinators~\cite{gll-combinators} are an
alternative to Scala's original parser combinators.  The GLL algorithm
allows left-recursion and ambiguity, and also provides more elegant
handling of semantic actions.
Note that the parser combinator libraries in Scala themselves are 
internal DSLs for writing grammars.
%
A yet different library is Parboiled2, a high-performance Parsing Expression
Grammar library~\cite{parboiled}. It uses macros to generate a parser at
compile time from a grammar written in an internal Scala DSL.
 
\subsubsection*{Example: Data automata}

{\em Data Automata} (\daut{})
\cite{havelund-isola-2014} monitor program executions at run time;
execution traces are checked against specifications in Daut's logic.
A trace is a sequence of events, each of which is a named tuple of
values. The logic is defined using Scala's original parser combinators~\cite{parser-combinators}. Assume as an example that a multi-threaded program has been instrumented to emit events reflecting the acquisition and release of
locks: $\mathit{acquire}(t,l)$ and $\mathit{release}(t,l)$ represent
events where thread $t$ acquires or releases lock $l$, respectively.
A simple way of detecting deadlock potentials
between two threads is to monitor whether threads take locks in a
circular manner: one thread acquiring some lock $l_1$ and then some
lock $l_2$ without having released $l_1$, and another thread acquiring
these two locks in the opposite order. Even if a deadlock does not
occur, the circular pattern represents a potential for it to occur.
The monitor shown in Figure \ref{fig:ext-lock-order-monitor},
formulated in the DSL named \daut, detects such potentials.

\begin{figure}
\begin{tightcenter}
\includegraphics[scale=\scaleFactor]{listings/LockOrder}
\end{tightcenter}
\caption{External \daut{} DSL: lock order monitor.
\label{fig:ext-lock-order-monitor}}
\end{figure}

Constructing a parser for a language usually begins with
designing an abstract syntax: data structures that represent the
result of parsing. In Scala we usually define such as case
classes, as shown in Figure~\ref{fig:ext-daut-ast-grammar} (top) for
the first few top-level case classes for this DSL.\footnote{A case class 
  in Scala is like a class with the
  additional properties that it supports pattern matching over objects
  of the class, that the {\bf new} keyword is not needed to create
  objects of the class, and equality is pre-defined based on the
  arguments.}
In this example, a
specification is a list of automata; an automaton has a name and
contains a lists of states. A state is defined by a collection of
state modifiers (specifying the kind of state, for example whether it is
an initial state or final state); a name; possible formal
parameters (states can be parameterized with data); and transitions. A
transition in turn consists of a pattern that must successfully match
an incoming event, and an optional condition which must evaluate to
true, for the transition to be taken, resulting in the right-hand side
(a state expression) to be executed.

\begin{figure}
\begin{tabular}{l}
\begin{minipage}[t]{.98\linewidth}
\textbf{AST.scala:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/ExternalDautAST}\\
\vspace{.25\baselineskip}
\textbf{Grammar.scala:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/ExternalDautGrammar}\\
\end{minipage}
\end{tabular}
\caption{External \daut{} DSL: part of the Scala AST and parser combinator grammar. 
\label{fig:ext-daut-ast-grammar}}
\end{figure}

%\begin{figure}
%  \begin{tightcenter}
%    \includegraphics[scale=\scaleFactor]{listings/ExternalDautAST}
%    \caption{External DSL: part of case classes defining abstract syntax.
%      \label{fig:ext-abstract-syntax}}
%  \end{tightcenter}
%\end{figure}

Figure \ref{fig:ext-daut-ast-grammar} (bottom) shows part of the parser expressed using
Scala's parser combinator library. The parser generates an
abstract syntax tree (AST) in the above-mentioned data structure.
%
A \code{specification} is a \code{Parser}, which when applied to an
input (as in \code{specification(}$\mathit{input}$\code{)})
produces an object of the (case) class \code{Specification}. 
A \code{specification} consists
of zero or more (\code{rep}) \code{automaton}. The code after the
\verb+^^+ symbol is a function, which takes as argument the parsed
sub-data structures and returns the \code{Specification} object, in
this case obtained by a transformation.  An
\code{automaton} is introduced by the keyword \code{monitor}, followed
by an identifier, and a block
delimited by \code{\{} and \code{\}}, which contains a sequence
of transitions and a sequence of state definitions.
%
Finally, a transition consists of a pattern, an optional
guard condition, and one or more target states following the arrow.
Transitions at the top level of a monitor represent an initial state.
%
The symbols 
\raisebox{-0.55ex}{\texttt{\~}}\verb+>+,
\verb+<+\raisebox{-0.55ex}{\texttt{\~}},
and 
\raisebox{-0.55ex}{\texttt{\~}}
are methods in the parser combinator DSL
that are used to represent spaces. This illustrates one of the
drawbacks of using an internal DSL such as parser combinators: the
notation can get slightly inconvenient, lowering readability.

%\begin{figure}
%  \begin{tightcenter}
%    \includegraphics[scale=\scaleFactor]{listings/ExternalDautGrammar}
%    \caption{External DSL: part of grammar expressed using parser combinators.
%      \label{fig:ext-grammar}}
%  \end{tightcenter}
%\end{figure}

%\begin{figure}
%\begin{small}
%\begin{framed}
%\begin{lstlisting}[language=scala]
%  def parse(file: String): Option[Specification] = {
%    val reader = new java.io.FileReader(file)
%    parseAll(specification, reader) match {
%      case Success(spec, _) =>
%        if (wellformed(spec)) {
%          return Some(spec)
%        } else {
%          return None
%        }
%      case fail @ NoSuccess(_, _) =>
%        println(fail)
%        return None
%    }
%  }
%\end{lstlisting}
%\end{framed}
%\end{small}
%\caption{External DSL: the parser function}
%\label{fig:ext-parser-function}
%\end{figure}

\subsection{The parser tool approach}

In the parser tool approach, a tool generates the parser program from a grammar.
Parser tool frameworks that can be used with
Scala include ScalaBison~\cite{scala-bison} and
ANTLR~\cite{antlr}. ScalaBison \cite{scala-bison} is a Scala parser
generator, and hence allows Scala code to be written directly as the
actions generating Scala objects.
ANTLR \cite{antlr} is Java-based and generates a parse tree as
a Java object. This then has to be traversed (visited) by a Scala
program and translated into a Scala tree.
%This is possible since Scala can import and use Java classes and methods,
%although it is more cumbersome.
The advantage of ANTLR is its robust parsing algorithm. We use ANTLR
%as our framework
for implementing the \Klang{} language---a SysML-inspired
language for specifying both high-level system descriptions as well as
low-level implementations of a system.
Note that ANTLR itself provides an external DSL for writing grammars.

\begin{figure}
\begin{tabular}{l}
\begin{minipage}[t]{.98\linewidth}
\textbf{Example.k:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/K}\\
\vspace{.25\baselineskip}
\textbf{Grammar.g4:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/K-ANTLR}\\
\end{minipage}
\end{tabular}
\caption{External \Klang{} DSL: example of a model and \antlr{} grammar. 
\label{fig:ext-K-example-grammar}}
\end{figure}

%\begin{figure}
%\begin{tightcenter}
%\includegraphics[scale=\scaleFactor]{listings/K}
%\end{tightcenter}
%\caption{External DSL: An example of a model in \Klang{}.
%\label{fig:ext-K-example}}
%\end{figure}

\subsubsection*{Example: The \Klang{} language\label{sec:ext-k}}

Figure~\ref{fig:ext-K-example-grammar} shows an example model
in \Klang{}. Class \code{Instrument}
specifies the instrument id, name, and power level.
Class \code{Spacecraft} models the spacecraft, which
contains several instruments with a unique
id, as specified by constraint \code{InstrumentsCount}.
Definition~\code{SpacecraftInstrument}
specifies that each \code{Spacecraft} can be \emph{associated} with a
maximum of 10 instruments, i.\,e., each \code{Instrument} contains a
reference to the \code{Spacecraft} and the \code{Spacecraft} contains
references to all instruments that are a \emph{part} of it.

Using ANTLR, we create a tool chain to parse and process
\Klang{} artifacts. ANTLR accepts grammars specified in \emph{Extended
  Backus-Naur-Form} (see Figure~\ref{fig:ext-K-example-grammar}).%
%\footnote{Parser combinators can be regarded as DSLs for writing parsers
%in a similar style.}
Patterns are specified using rules and sub-rules that can be marked optional
(\texttt{?})  and repetitive (\texttt{*}, \texttt{+}).
Each rule can also be named using the \texttt{\#}
notation at the end of a rule. Keywords are specified in single
quotes. In the example, rule \code{classDeclaration} specifies how
classes are defined by an identifier, an
optional \emph{extends} declaration, and an optional class
member declaration list enclosed within curly brackets. Similarly, we
define the grammar for associations. The \code{expression} rule
specifies how expressions can be constructed in \Klang{}. Rule
precedence is determined by the order of the occurrence of the rules.

%\begin{figure}
% java2eps currently does not support Unicode
%\begin{tightcenter}
%\includegraphics[scale=\scaleFactor]{listings/K-ANTLR}
%\end{tightcenter}
%\caption{External DSL: the grammar of K.
%\label{fig:ext-K-grammar}}
%\end{figure}

Given a grammar, ANTLR produces a lexer and parser (in Java).
ANTLR further enables one to create a \emph{visitor}
to visit the nodes in the generated parse tree. We implement a visitor
in Scala by importing the required Java libraries and \emph{stitching}
the code together to access the ANTLR parse tree in
Scala. Figure~\ref{fig:ext-K-ast-visitor} shows a snippet of the visitor
for the \Klang{} language. The visitor makes use of classes defined in
the K AST, also shown in Figure~\ref{fig:ext-K-ast-visitor}. Both snippets
correspond to visiting expression nodes in the parse tree and creating
expression declarations in the AST. For example, the visitor function
\code{visitDotExp} takes as input a context \code{ctx} of type
\code{DotExpContext}. This is used to extract the
expression \code{e} and the identifier \code{ident} from the ANTLR
parse tree. Together, these are used to create an instance of
\code{DotExp}, which is defined in the \Klang{} Scala AST.
%
The \Klang{} Scala visitor produces the complete AST, which can be used for
code generation and analysis. We currently use the AST to transform
\Klang{} code to JSON and
back. The robustness of ANTLR is a benefit, but there is also
significant effort involved in creating the visitor that produces
the Scala AST from the Java AST. 
A small change in the grammar can produce a cascading series of
changes to the visitor.

\begin{figure}
\begin{tabular}{l}
\begin{minipage}[t]{.98\linewidth}
\textbf{Visitor.scala:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/ExternalKVisitor}\\
\vspace{.25\baselineskip}
\textbf{AST.scala:}\\
\vspace{2mm}
\includegraphics[scale=\scaleFactor]{listings/ExternalKAST}\\
\end{minipage}
\end{tabular}
\caption{External DSL: part of the ANTLR visitor and AST for \Klang{}. 
\label{fig:ext-K-ast-visitor}}
\end{figure}

