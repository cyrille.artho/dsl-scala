---------- Forwarded message ----------
From: ICFEM 2015 <icfem2015@easychair.org>
Date: Thu, Jul 2, 2015 at 3:13 AM
Subject: ICFEM 2015 Camera-Ready paper 2
To: Klaus Havelund <havelund@gmail.com>


Paper   : 2
Authors : Cyrille Valentin Artho, Klaus Havelund, Rahul Kumar and Yoriyuki Yamagata
Title   : Domain-Specific Languages with Scala

You have already received the notification and comments by the reviewers in a previous
email. Please take them carefully into account when preparing your
camera-ready paper for the proceedings. The final paper and the
signed copyright form are due on

  July 15, 2015.

This is a firm deadline for the production of the proceedings.

1. FINAL PAPER: Please submit the files belonging to your
camera-ready paper using your EasyChair author account. Follow the
instructions after the login for uploading two files:

  (a) either a zipped file containing all your LaTeX sources
      or a Word file in the RTF format, and
  (b) PDF version of your camera-ready paper.

The page limit is 16 pages. However, for the proceedings we can accept one additional
page beyond the page limit for the main text.

Please follow strictly the author instructions of Springer-Verlag
when preparing the final version:

  http://www.springer.de/comp/lncs/authors.html

Our publisher has recently introduced an extra control loop: once
data processing is finished, they will contact all corresponding
authors and ask them to check their papers. We expect this to happen
shortly before the printing of the proceedings. At that time your
quick interaction with Springer-Verlag will be greatly appreciated.

2. COPYRIGHT: Please upload a signed and completed copyright form to
us as soon as possible. The Springer copyright forms
can be found at

 http://www.springer.com/computer/lncs?SGWID=0-164-6-793341-0

It is sufficient for one of the authors to sign the copyright form.
You can scan the form into PDF or any other standard image format.

We expect that at least one author from each accepted paper will register as a full registrant for July 31, 2015 and attend/present the work at the conference. Failure to do so will result in the paper being removed from the proceedings.

We greatly appreciate your cooperation in these matters. Thank you
again for your contribution to ICFEM 2015.

Sincerely,
The ICFEM 2015 Chairs
